'use strict';
//Теоретический вопрос
// Опишите своими словами, что такое метод обьекта
//
// Задание
// Реализовать функцию для создания объекта "пользователь".
//
// Технические требования:
// Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
// При вызове функция должна спросить у вызывающего имя и фамилию.
// Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
//
// Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя,
// соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin().
// Вывести в консоль результат выполнения функции.
//
// Не обязательное задание продвинутой сложности:
// Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.
//
// Solution:

function createNewUser() {
    let newUser = {
        firstName,
        secondName,
        set setFirstName(value) {
            this.firstName = value;
        },
        set setSecondName(value) {
            this.secondName = value;
        }
    };
    newUser.getLogin = function () {
        return (this.firstName.charAt(0) + this.secondName).toLowerCase();
    };
    return newUser;
}

let newUser = createNewUser(prompt(`What is your First Name?`, ``), prompt(`What is your Second Name?`, ``));
// let newUser = createNewUser('nikita', 'koval');

console.log(newUser.getLogin);
console.log(newUser);